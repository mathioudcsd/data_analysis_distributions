fid = fopen('Syslog_data.txt');
arpq = textscan(fid, '%d %d %d %s %s %*[^\n]');
fclose(fid);

f = find(arpq{2} == 410 & strcmp(arpq{5},'Associated')); 
for i = 1:size(f)  
  timest(i,:) = arpq{1}(f(i));
end

diffs=diff(timest); 

%%%%%%%%%%%%%% i %%%%%%%%%%%%%
poisson_dist = envelope_qqplot(double(diffs), 'poisson', 1000);


%%%%%%%%%%%%%%  ii %%%%%%%%%%%%
pareto_dist = envelope_qqplot(double(diffs), 'pareto', 1000);


%%%%%%%%%%%%%%  iii %%%%%%%%%%%%
unique_APs = unique(arpq{2});

[ev_types b ev_id] = unique(arpq{5});

% Find the integer id that corresponds to the event 'Associated'.
for k=1:1:size(ev_types, 1)
    if strcmp(ev_types(k), 'Associated') == 1  
        association_ev_id= k;
    end
end

% Find the lines that correspond to the event 'Associated'.
lines = ev_id == association_ev_id;


associated_ev_all_APs = arpq{2}(lines); %ola ta APs poy exoyne associated event
unique_assoc_APs = unique(associated_ev_all_APs); % ta unique APs poy exoyne associated event
times_unique_APs_found = histc( associated_ev_all_APs, unique_assoc_APs); % oi fores poy exei emfanistei to kathe unique AP mesa se ola APs poy exoyne associated

%gia na vroume pia apo ta unique APs exoun associated enent more than 2 times 
j=1;
for i=1:1:size(unique_assoc_APs)
    if(times_unique_APs_found(i) >= 2)
         final_APs(j,:) = unique_assoc_APs(i);
        j=j+1;
    end
end

%gia kathe AP pou telika meletame kratame ta interarival times kai vazoume miden sta upolipa kelia
temp =0 ;
for i=1:1:size(final_APs)
    clearvars temp;
    for j=1:1:size(arpq{1})
        if(arpq{2}(j) == final_APs(i) && strcmp(arpq{5}(j), 'Associated') )
            temp(j,:) = arpq{1}(j);
        else
            temp(j,:) = 0;
        end
    end  
    inter_times(:,i)= int32(temp) ;
end


%upologizoume ta lambda gia ta interarrivals poy exoyme vrei
 clearvars my_lambda;
for i=1:1:size(final_APs)
    my_pd(i) = fitdist(diff(nonzeros(double(inter_times(:,i)))),'Poisson');
    my_lambda(i,:) = my_pd(i).lambda;
end

figure;
ecdf(my_lambda);

%%%%%%%%%%%%%%%%%%%%%%  iv  %%%%%%%%%%%%%%%%%%%%%% 

%kanoume ksana oli th diadikasia gia ta distr kai ta lambdas giati ams
%endiaferei na kratisoume kai tis times pou htan miden kai thelame na
%agnoisoume sto parapanw cdf
all_temp =0 ;
for i=1:1:size(unique_assoc_APs)
    clearvars temp;
    for j=1:1:size(arpq{1})
        if(arpq{2}(j) == unique_assoc_APs(i) && strcmp(arpq{5}(j), 'Associated') )
            all_temp(j,:) = arpq{1}(j);
        else
            all_temp(j,:) = 0;
        end
    end  
    all_inter_times(:,i)= int32(all_temp) ;
end

clearvars my_lambda;
for i=1:1:size(unique_assoc_APs)
    if(size(nonzeros(all_inter_times(:,i))) <2 )
        all_my_pd(i) = makedist('Poisson');
        all_my_pd(i).lambda = 0;
        all_my_lambda(i,:) = 0;
    else
        all_my_pd(i) = fitdist(diff(nonzeros(double(all_inter_times(:,i)))),'Poisson');
        all_my_lambda(i,:) = all_my_pd(i).lambda;
    end
end

%gia ola ta associated events pou exoun uparksei tha vroume to distr kai to lambda aytou
temp_iv =0 ;
for j=1:1:size(arpq{1})
    if(strcmp(arpq{5}(j), 'Associated') )
        temp_iv(j,:) = arpq{1}(j);
    else
        temp_iv(j,:) = 0;
    end
end

my_pd_iv = fitdist(double(diff(nonzeros(int32(temp_iv)))),'Poisson');
my_lambda_iv = my_pd_iv.lambda;

%vriskoume to pososto twn parameters lambda pou einai mikrotera apo ta lambda tou proigoumenou erwtimastos 
my_sum = sum(my_lambda_iv < all_my_lambda);
comp = my_sum / size(all_my_lambda, 1);


%%%%%%%%%%%%%%%%%%%%%%  v  %%%%%%%%%%%%%%%%%%%%%

samples = 10000;

figure;
ecdf(exprnd(my_lambda_iv, samples,1));












