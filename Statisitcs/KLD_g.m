function dist=KLD_g(X,Y,Nbins,Sbin,Gmin)
%Like KLD_u, but passing as arguments the number of bins and bin size
                 
  XY = union(X,Y);
  
  for i=1:Nbins
      grid(i)=Gmin+(i-1)*Sbin;
  end
      
    Hx=hist(X,grid);
    Hy=hist(Y,grid);
    
    for i=1:length(Hx)
       %if (Hx(i)==0)
            Hx(i)=Hx(i)+eps;
        %nd
    end
    for i=1:length(Hy)
      % if (Hy(i)==0)
            Hy(i)=Hy(i)+ eps;
       % end
    end
    
    Nx=0;
    for i=1:length(Hx)
        Nx=Nx+Hx(i);
    end
    Px=Hx./Nx;
    
    Ny=0;
    for i=1:length(Hy)
        Ny=Ny+Hy(i);
    end
    Py=Hy./Ny;
   
   sum=0.0;
   for i=1:Nbins
     sum=sum+Px(i)*log(Px(i)/Py(i));
   end
   
   dist=sum;
   %dist2 = KLDiv(Px,Py);
   %if (dist> dist2 || dist<dist2)
    %   error('Oi apostaseis einai %d kai %d',dist, dist2);
   %end
   
end