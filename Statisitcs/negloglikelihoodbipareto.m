function y=negloglikelihoodbipareto(para);
%NEGLOGLIKELIHOODBIPARETO Negative Log Likelihood of Bipareto Distribution
%with standard k=1;
%
%  y=negloglikelihoodbipareto(data, para);
%
%data is the bipareto samples,
%para is a column vector, which will be (alpha, beta, c)';
%
%2004 (C) Copyright Lingsong Zhang(LSZHANG@unc.edu)

global data;
para=max(para, 0);
if min(para)==0;
    ytemp=-2^1000;
else 
    temp=(para(2)-para(1))*log(1+para(3))-(para(1)+1)*log(data)+(para(1)-para(2)-1)*log(data+para(3))+log(para(2)*data+para(1)*para(3));
    ytemp=sum(temp);
end
y=-1*ytemp;

