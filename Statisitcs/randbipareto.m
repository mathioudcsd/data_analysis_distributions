function y=randbipareto(N, alpha, beta, c, k);
%RANDBIPARETO random number (vector) generateor for bipareto distribution
%
% y=randbipareto(N, alpha, beta, c, k);
%
%Generate column random vector (length=N) of BiPareto Distribution
%
% make sure the following files are in the same dirctory or paths
%  cdfbipareto.m
%  icdfbipareto.m
% 
%Copyright(c) 2004 Lingsong Zhang(LSZHANG@unc.edu), Haipeng Shen(haipeng@email.unc.edu)

U=rand(N,1);
tic
y=zeros(N, 1);
for i=1:N;
    y(i)=icdfbipareto(U(i), alpha, beta, c, k);
%    if( y(i) == -1 ) 
%        y(i)=icdfbipareto(U(i), alpha, beta, c, k);
%        disp '******************** Repeating *******************\n';
%    end
end;
toc