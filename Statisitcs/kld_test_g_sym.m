function dist=kld_test_g_sym(indata, distribution,Nbins,Sbin,Gmin)
%%
%Differs from the kld_test_sym only in the additional three parameters Nbins,Sbin,Gmin
%Calls the KLD_g
    fs = indata;
    values = length(fs);
    paramhat_index = 1;
    
    %===============================
    % create simulation data
    %===============================
    
    switch lower(distribution)
        case {'weibull'}
           [paramhat] = wblfit( fs );
           wdata0 = wblrnd(paramhat(1,1),paramhat(1,2),values,1 );
          
        case {'lognormal'}
           [paramhat] = lognfit( fs );
           wdata0 = lognrnd(paramhat(1,1),paramhat(1,2),values,1 );
        case {'exponential'}
           [paramhat] = expfit( fs );
           wdata0 = exprnd(paramhat(1,1),values,1 );
          
        case {'extreme_value'}
           [paramhat] = evfit( fs );
           wdata0 = evrnd(paramhat(1,1),paramhat(1,2),values,1 );
        case {'generalized_extreme_value'}
           [paramhat] = gevfit( fs );
           wdata0 = gevrnd(paramhat(1,1),paramhat(1,2),paramhat(1,3),values,1 );
        case {'pareto'}
           paramhat = gpfit( fs -min(fs)+eps );
           wdata0 = gprnd(paramhat(1,1), paramhat(1,2), min(fs), values, 1);
           %[paramhat] = gpfit( fs );
           %wdata0 = gprnd(paramhat(1,1),paramhat(1,2),min(fs),values,1 );
        case {'gamma'}
           [paramhat] = gamfit( fs );
           wdata0 = gamrnd(paramhat(1,1),paramhat(1,2),values,1 );
          
        case {'rayleigh'}
           [paramhat] = raylfit( fs );
           wdata0 = raylrnd(paramhat(1,1),values,1 );
        case {'bipareto'}
            [paramhat] = fitBiPareto_no_displ( fs, 60, 'FS');
           wdata0 = floor(randbipareto(500, paramhat(1,1), paramhat(1,2), paramhat(1,3), min(fs)));
           % these functions elapse at ~8ms
       otherwise
           error('Invalid distribution');
    end
    
    dst=KLD_g(wdata0,fs,Nbins,Sbin,Gmin);
    rdst=KLD_g(fs,wdata0,Nbins,Sbin,Gmin);
    dist=(dst+rdst)/2;
    
end