function dist=kld_test_all_g_sym(rd,Nbins)
 % The only difference from the kld_test_all_sym is that it passes as arguments to the kld_test for the creation of the histograms, the number of bins, bin size, and grid min value      
n=30;
Gmin=min(rd);
Gmax=max(rd);
%Nbins=30;
Sbin=(Gmax-Gmin)/Nbins;

    for i=1:n
       s(i)=kld_test_g_sym(rd,'rayleigh',Nbins,Sbin,Gmin);
    end
    dist(1,1)=median(s);
    dist(1,2)=std(s);
    
    for i=1:n
       s(i)=kld_test_g_sym(rd,'weibull',Nbins,Sbin,Gmin);
    end
    
    dist(2,1)=median(s);
    dist(2,2)=std(s);
    
    for i=1:n
       s(i)=kld_test_g_sym(rd,'lognormal',Nbins,Sbin,Gmin);
    end
    dist(3,1)=median(s);
    dist(3,2)=std(s);
    
    for i=1:n
       s(i)=kld_test_g_sym(rd,'pareto',Nbins,Sbin,Gmin);
    end
    dist(4,1)=median(s);
    dist(4,2)=std(s);
    
    for i=1:n
       s(i)=kld_test_g_sym(rd,'gamma',Nbins,Sbin,Gmin);
    end
    dist(5,1)=median(s);
    dist(5,2)=std(s);
    
    for i=1:n
       s(i)=kld_test_g_sym(rd,'exponential',Nbins,Sbin,Gmin);
    end
    dist(6,1)=median(s);
    dist(6,2)=std(s);  
end