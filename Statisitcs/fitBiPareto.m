
function [ para, flag, fval ] = fitBiPareto( data, convlvl, vartype )
%%
% Description
% 	fit the distribution data to BiPareto distribution, the parameters are fitted via maximum likelihood
% Input data
% 	data : input data vector
%	convlvl :  optimization starts at the point convlvl and finds a local minimum x of the function negloglikelihoodbipareto
%	vartype : string containing 
%			'FPS' if flows per sessions are used as input data
%			'FS' if flowsizes are used as input data.
% Output data
%	para : distribution parameters
%	fval : the value of the objective function fun at the solution x.
%	
%	flag:
%		1	fminsearch converged to a solution x.
%		0	Maximum number of function evaluations or iterations was reached.
%		-1	Algorithm was terminated by the output function.
% e.g. [ paraFPS ] = fitBiPareto( FLINFO_flows_persession_outexcl_AP(:,2), 60, 'FPS');


%%  Preprocess data
    data = data(data>0);
    k=min(data);
    if( strcmp( vartype, 'FS'))
        data=data/k;  
    elseif( strcmp( vartype, 'FPS'))
        % do not normalize data
    else
        disp 'Invalid variable type.Exiting.';
        exit 0;
    end
    global data;
%%   Plot data
    figure;
    [f x]=ecdf(data,'function','survivor');
    [xx yy] = stairs(x, f);loglog(xx,yy,'k-','LineWidth',1);hold on;
    hold on;
    
%%  Find minimum of unconstrained multivariable function using derivative-free method
   %[para, fval, flag, output]=fminsearch(@negloglikelihoodbipareto, [0.01, 2, convlvl], optimset('Display', 'iter', 'TolFun', 1e-5));
   [para, fval, flag, output]=fminsearch(@negloglikelihoodbipareto, [0.01, 2, convlvl], optimset('Display', 'iter'));

    %superimpose the 20 points from the true CCDF on the log10-log10 plot
    x = min(log10(data)):range(log10(data))/20:max(log10(data));
    xx = power(10,x);
    loglog(xx,1-cdfbipareto(xx, para(1), para(2), para(3), 1),'ro');
    plot(xx, power(10,-log10(xx)*(para(3)/(1+para(3))*para(1)+para(2)/(1+para(3)))), 'm-')
    %axis manual
    plot(xx, power(10,-log10(xx)*para(2)-(para(1)-para(2))*log10(1+para(3))), 'm-');
    ylim([1e-4 1.5]);
    
%%  Print axis/ labels/ ticks
    %xlabel(strcat('Number of in-session ', ' ',vartype),'FontSize',14);
    xlabel(strcat(vartype),'FontSize',14);
    ylabel('CCDF','FontSize',14);
    set(gca,'FontSize',10);
    % build the xticklabels
    xticklab = get(gca,'XTickLabel');
    for j=1:length(xticklab)
        xticklab2{j} = num2str(k*power(10,str2num(xticklab(j))));
    end
    set(gca,'XTickLabel',xticklab2);
    grid on;set(gca,'XMinorGrid','off');set(gca,'YMinorGrid','off');
    leg2 = strcat('BiPareto(',num2str(para(1)),', ',num2str(para(2)),', ',num2str(para(3)),', ',num2str(k),')');
    legend('Empirical CCDF', leg2, 3);
    title(strcat('Bipareto fit -', ' ', vartype),'FontSize',14);
    
    f = figure(1);
    figname = strcat('./Distribution_Fitting_OE/Bipareto_fit_',vartype,'[', int2str(convlvl),']')
%    print(f,'-djpeg', figname);
    %close(f);
    
%%  Clear data
    clear f x xx yy;
    clear global;
end
