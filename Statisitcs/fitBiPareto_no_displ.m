
function [ para, flag, fval ] = fitBiPareto_no_displ( data, convlvl, vartype )
%%
% Description
% 	fit the distribution data to BiPareto distribution, the parameters are fitted via maximum likelihood
% Input data
% 	data : input data vector
%	convlvl :  optimization starts at the point convlvl and finds a local minimum x of the function negloglikelihoodbipareto
%	vartype : string containing 
%			'FPS' if flows per sessions are used as input data
%			'FS' if flowsizes are used as input data.
% Output data
%	para : distribution parameters
%	fval : the value of the objective function fun at the solution x.
%	
%	flag:
%		1	fminsearch converged to a solution x.
%		0	Maximum number of function evaluations or iterations was reached.
%		-1	Algorithm was terminated by the output function.
% e.g. [ paraFPS ] = fitBiPareto( FLINFO_flows_persession_outexcl_AP(:,2), 60, 'FPS');


%%  Preprocess data
    data = data(data>0);
    k=min(data);
    if( strcmp( vartype, 'FS'))
        data=data/k;  
    elseif( strcmp( vartype, 'FPS'))
        % do not normalize data
    else
        disp 'Invalid variable type.Exiting.';
        exit 0;
    end
    global data;

    
%%  Find minimum of unconstrained multivariable function using derivative-free method
   %[para, fval, flag, output]=fminsearch(@negloglikelihoodbipareto, [0.01, 2, convlvl], optimset('Display', 'iter', 'TolFun', 1e-5));
   [para, fval, flag, output]=fminsearch(@negloglikelihoodbipareto, [0.01, 2, convlvl], optimset('Display', 'iter'));

    
end
