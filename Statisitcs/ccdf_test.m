function ccdf_test( data , distribution, log)
% This function plots the empirical complementary comulative 
% distribution functtion of a dataset X.
%
% input parameters:
%   data [Nx1]: A column vector that contains the samples of the dataset X.
%   log  [1x1]: If the value of this parameter is 1 the ccdf is plotted in 
%               log-log scale. Otherwise the ccdf is plotted in linear scale.


switch lower(distribution)
        case('rayleigh')
           [paramhat] = raylfit( data );
           wdata = raylrnd(paramhat(1,1), 100000, 1);
        case('weibull')
           [paramhat] = wblfit( data );
           wdata = wblrnd(paramhat(1,1),paramhat(1,2),100000,1 );
        case('lognormal')
           [paramhat] = lognfit( data );
           wdata = lognrnd(paramhat(1,1),paramhat(1,2), 100000,1 );
        case('pareto')
           [paramhat] = gpfit( data - min(data)+ eps);
           wdata = gevrnd(paramhat(1,1),paramhat(1,2),min(data), 100000,1 );
        case('gamma')
           [paramhat] = gamfit( data );
           wdata = gamrnd(paramhat(1,1),paramhat(1,2), 100000,1 );
        case('exponential')
            [paramhat] = expfit( data );
           wdata = exprnd(paramhat(1,1), 100000,1 );
end

[F1 X1] = ecdf(data);
[F2 X2] = ecdf(wdata);

figure;
set(gcf,'color',[1 1 1]);
if (log)
    loglog(X1, 1-F1, '--', X2, 1-F2, '-');
else
    plot(X1, 1-F1, '--', X2, 1-F2, '-');
end
xlabel('x');
ylabel('F_c(x)');
title('Empirical ccdf');
message = [distribution ' distribution'];
legend(message, 'empirical distribution');

end

