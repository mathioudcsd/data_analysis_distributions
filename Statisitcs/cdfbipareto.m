function y=cdfbipareto(x, alpha, beta, c, k)
%CDFBIPARETO Cumulative distribution function of BiPareto distribution
%   By Lingsong Zhang(LSZHANG@unc.edu)
%
% F(x)=1-(x/k)^{-alpha}((x/k+c)/(1+c))^{alpha-beta}
%
% y=cdfbipareto(x, alpha, beta, c, k)
%
%Copyright(c) 2004 Lingsong Zhang(LSZHANG@unc.edu), Haipeng Shen(haipeng@email.unc.edu)

y=1-(x./k).^(-1*alpha).*((x./k+c)./(1+c)).^(alpha-beta);

