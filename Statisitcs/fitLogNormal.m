%%  BiParetofit_flownum
%   fit the distribution data to LogNormal distribution, the parameters are fitted via maximum likelihood
function [ para ] = fitLogNormal( data_ses_inter )
%%
% Description
% 	fit the distribution data to LogNormal distribution, the parameters are fitted via maximum likelihood
% Input data
% 	data_ses_inter : input data vector
% Output data
%	para : distribution parameters


     data_ses_inter = data_ses_inter(data_ses_inter>0);
  
%%   Plot data_ses_inter
    figure;
    [f x] = ecdf(data_ses_inter,'function','survivor');
    [xx yy] = stairs(x, f);loglog(xx,yy,'k-','LineWidth',1);hold on;
    
%%  Find minimum of unconstrained multivariable function
    para = MLE(data_ses_inter,'Distribution','Lognormal');
    
    %superimpose the 20 points from the true CCDF on the log10-log10 plot
    x = min(log10(data_ses_inter)):range(log10(data_ses_inter))/20:max(log10(data_ses_inter));
    xx = power(10,x);
 
    loglog(xx,1-logncdf(xx, para(1), para(2)),'ro');
    %plot(xx, power(10,-x*para(2)-(para(1)-para(2))*log10(para(3))+para(2)*log10(1)), 'm-');
    ylim([1e-4 1.5]);
    
    xlabel('In-session flow interarrival','FontSize',14);
    ylabel('CCDF','FontSize',14);
    set(gca,'FontSize',10);
    
%%  Plot results
    grid on;set(gca,'XMinorGrid','off');set(gca,'YMinorGrid','off');
    leg2 = strcat('Lognormal(',num2str(para(1)),', ',num2str(para(2)),')');
    legend('Empirical',leg2,3);
       
    f=figure(1);
    figname = strcat('./Distribution_Fitting_OE/flowintvals_','orig');
    print(f,'-depsc',figname);
    %close(f);
  
    length(data_ses_inter)
    
%%  Clear data
    clear f x xx yy;
end
