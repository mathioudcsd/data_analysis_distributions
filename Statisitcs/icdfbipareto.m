function y=icdfbipareto(x, alpha, beta, c, k, yup, ylow)
%% ICDFBIPARETO inverse function of BiPareto cumulative distribution function
% just a scalar function of the inverse function
% 
% y=icdfbipareto(x, alpha, beta, c, k);
%
% make sure the following file(s) in the same directory
%   cdfbipareto.m
% And make sure x is just a scalar
%
%Copyright(c) 2004 Lingsong Zhang(LSZHANG@unc.edu), Haipeng Shen(haipeng@email.unc.edu)

%upper_thres = 1E9;
%upper_thres = 1E11;
%upper_thres = 1E21;
upper_thres = 1E41;
%%

%first check whether x is in the seeking range we can do
if length(x)>1;
    error('x must be a scalar');
end;
if nargin<=5;
ylow=k;
yup=upper_thres;
xlow=0;
xup=cdfbipareto(upper_thres, alpha, beta, c, k);
else xup=cdfbipareto(yup, alpha, beta, c, k);
    xlow=cdfbipareto(ylow, alpha, beta, c, k);
end;
%%
stop=0;
if x<xlow;
    error_str = [ 'x is too much small (' int2str( x ) '<' int2str( xlow ) ')'];
    error(error_str);
elseif x>xup;
    error_str = [ 'x is out of the seeking area of the program (' int2str( x ) '>' int2str( xup ) ')'];
    error(error_str);
    %y = -1;
end;
%%
if x==xlow;
    y=ylow; %y=1e10;
    stop=1;
end;
if x==xup;
    y=yup; %y=k;
    stop=1;
end;
%%
while (cdfbipareto(yup, alpha, beta, c, k)>x) && (cdfbipareto(ylow, alpha, beta, c, k)<x) && (stop==0);
   ysearch=(ylow+yup)/2;
   if cdfbipareto(ysearch, alpha, beta, c, k)==x;
       y=ysearch;
       stop=1;
   elseif cdfbipareto(ysearch, alpha, beta, c, k)>x;
       yup=ysearch;
   else ylow=ysearch;
   end;
   if yup-ylow<=1E-9;
       y=(yup+ylow)/2;
       stop=1;
   end;
end;
