fid = fopen('cell0_2.txt');
arpq = textscan(fid, '%f %s %d %*[^\n]');
fclose(fid);

k_84a0 = find(strcmp(arpq{2},'00:11:21:61:84:a0')); 
for i = 1:size(k_84a0)  
  cd_84a0(i,:) = arpq{3}(k_84a0(i));
end

figure;
ecdf(cd_84a0);
ylabel('probability');
xlabel('db');
title('CDF of the signal strength of AP 84:a0 ');

%%%
k_1e30 = find(strcmp(arpq{2},'00:11:93:03:1e:30')); 
for i = 1:size(k_1e30)  
  cd_1e30(i,:) = arpq{3}(k_1e30(i));
end

figure;
ecdf(cd_1e30);
ylabel('probability');
xlabel('db');
title('CDF of the signal strength of AP 1e:30 ');

%%%%%%%%%%%%%%%%%%% ii %%%%%%%%%%%%%%%%%%%
mean_84a0 = mean(cd_84a0);
max_84a0 = max(cd_84a0);
min_84a0 = min(cd_84a0);

mean_1e30 = mean(cd_1e30);
max_1e30 = max(cd_1e30);
min_1e30 = min(cd_1e30);

figure;
plot_84a0 = [mean_84a0 max_84a0 min_84a0];
plot_84a0_names = categorical( {'mean','max','min'});
barh(plot_84a0_names, plot_84a0);
ylabel('Protocols');
xlabel('signal strength, db');
title('Mean, max, min values Graph of 84:a0');

figure;
plot_1e30 = [mean_1e30 max_1e30 min_1e30];
plot_1e30_names = categorical( {'mean','max','min'});
barh(plot_1e30_names, plot_1e30);
ylabel('Protocols');
xlabel('signal strength, db');
title('Mean, max, min values Graph of 1e:30');

%%%%%%%%%%%%%%%%%%% iii %%%%%%%%%%%%%%%%%%%


norm_dist_840a = envelope_qqplot(-(double(cd_84a0)), 'normal', 1000);
norm_dist_1e30 = envelope_qqplot(-(double(cd_1e30)), 'normal', 1000);


